$(document).ready(function(){
    $('#menu').on('click','a', function (event) {
        event.preventDefault();
        let $id  = $(this).attr('href'),
            top = $($id).offset().top;
        $('body,html').animate({scrollTop: top}, 1500);
    });

    let $btnTop = $('.btn-top');
    $(window).on('scroll', function() {
        if($(window).scrollTop() >= 650){
            $btnTop.fadeIn();
        } else {
            $btnTop.fadeOut();
        }
    });

    $btnTop.on('click', function() {
        $('html, body').animate({scrollTop:0}, 900)
    });

    $('#accordion').click(function() {
        $('.accordion').slideToggle('slow');
    });
});